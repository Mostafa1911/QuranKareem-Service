# QuranKareem-Service
WebService للقرآن الكريم

# Base Url

https://mostafagad9090.000webhostapp.com/QuranService/GetQuran.php


# Service name 
   
   [GetQuran.php]()
   
# Documentaion
          Name : GetQuran.php 
          Parameters : reader_id (int)
          Output :  
                     reader_id = 1 
                     return القرآن الكريم بصوت الشيخ مشارى العفاسى
                     reader_id = 2 
                     return القرآن الكريم بصوت الشيخ ماهر المعيقلي
                     reader_id = 3 
                     return القرآن الكريم بصوت الشيخ أحــمد العجمى
                     reader_id = 4 
                     return القرآن الكريم بصوت الشيخ سعد الغامدى
                     reader_id = 5 
                     return القرآن الكريم بصوت الشيخ عبدالرحمن السديسى
                     reader_id = 6 
                     return القرآن الكريم بصوت الشيخ محــمد جـــبريــل
                     reader_id = 7 
                     return القرآن الكريم بصوت الشيخ عبدالــباسط عبدالصمد
                     reader_id = 8 
                     return القرآن الكريم بصوت الشيخ محمد صديق المنشاوى
                     reader_id = 9 
                     return القرآن الكريم بصوت الشيخ فـــــارس عـــباد
                     reader_id = 10 
                     return القرآن الكريم بصوت الشيخ وديــــع اليمني
                     reader_id = 11 
                     return القرآن الكريم بصوت الشيخ مـــحمد الطبلاوى
                     reader_id = 12 
                     return القرآن الكريم بصوت الشيخ أحــمد الحذيفى
                     reader_id = 13 
                     return القرآن الكريم بصوت الشيخ نــاصر القطامي
                     reader_id = 14 
                     return القرآن الكريم بصوت الشيخ عبدالله الجهنى
                     reader_id = 15 
                     return القرآن الكريم بصوت الشيخ ياسر الدوسري
                     reader_id = 16 
                     return القرآن الكريم بصوت الشيخ بندر بليلة
                     reader_id = 17
                     return القرآن الكريم بصوت الشيخ سعود الشريم 
                     reader_id = 18
                     return القرآن الكريم بصوت الشيخ إدريس أبكر 
                     reader_id = 19
                     return القرآن الكريم بصوت الشيخ محمود خليل الحصري 
                     reader_id = 20
                     return القرآن الكريم بصوت الشيخ محمد حسان   
                     reader_id = 21
                     return القرآن الكريم بصوت الشيخ أحمد خضر الطرابلسي
                     reader_id = 22
                     return القرآن الكريم بصوت الشيخ أبو بكر الشاطري
                     reader_id = 23
                     return القرآن الكريم بصوت الشيخ ياسر القرشي
                     reader_id = 24
                     return القرآن الكريم بصوت الشيخ عبدالله خياط
                     reader_id = 25
                     return القرآن الكريم بصوت الشيخ عادل ريان

# Apps service used in
  -  Nabta : 
          https://play.google.com/store/apps/details?id=education.mostafa.quranyapp.quranapp

# Try it on Post man

<img src="https://user-images.githubusercontent.com/25991597/65971742-f9314c00-e468-11e9-95f2-b459c6b4805f.PNG" width="700" height="400" />

